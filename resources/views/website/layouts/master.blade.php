
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$setting?$setting->website_name:''}} | @stack('title')</title>

  @include('website.includes.css')
</head>

<body>
<!-- start page-wrapper -->
<div class="page-wrapper">
@include('website.includes.header')
    <!-- start of hero -->

    @yield('content')

    @include('website.includes.footer')
</div>
<!-- end of page-wrapper -->

@include('website.includes.js')
</body>
</html>
