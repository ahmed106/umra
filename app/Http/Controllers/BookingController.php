<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Umra;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class BookingController extends Controller
{
    public function index()
    {
        return view('dashboard.bookings.index');

    }//end of index function

    public function data()
    {
        $bookings = Booking::with(['umra'])->get();

        $model = 'bookings';
        return DataTables::of($bookings)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" value="' . $raw->id . '" class="check_item">';
            })
            ->addColumn('actions', function ($raw) use ($model) {
                return view('dashboard.includes.actions', compact('model', 'raw'));
            })
            ->addColumn('photo', function ($raw) {
                return '<img src="' . $raw->umra->image . '">';
            })
            ->addColumn('title', function ($raw) {
                return $raw->umra->name;
            })
            ->addColumn('at', function ($raw) {

                return Carbon::parse($raw->created_at)->format('Y M d');
            })
            ->rawColumns(['check_item' => 'check_item', 'photo' => 'photo'])
            ->make(true);

    }//end of data function

    public function show($id)
    {

        $booking = Booking::with('umra')->where('id', $id)->first();
        return view('dashboard.bookings.show', compact('booking'));


    }//end of show function


    public function destroy(Booking $booking)
    {
        Booking::destroy($booking->id);
        return redirect()->route('bookings.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {

        Booking::destroy($request->items);
        return redirect()->route('bookings.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function
}
