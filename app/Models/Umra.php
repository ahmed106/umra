<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Umra extends Model
{
    protected $table = 'umras';
    protected $guarded = [];
    protected $appends = ['image'];

    public function photo()
    {
        return $this->morphOne(Photo::class, 'photoable');
    }//end of photos function

    public function getImageAttribute()
    {

        if ($this->photo == '') {
            return asset('default.svg');
        }

        return asset('images/umras/' . $this->photo->src);


    }//end of getImageAttribute function
}
