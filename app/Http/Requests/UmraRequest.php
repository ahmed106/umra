<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UmraRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();

    }

    public function onStore()
    {
        return [
            'name' => 'required|string',
            'details' => 'required|string',
            'price' => 'required|integer',
            'photo' => 'required|file',

            'start_at' => 'required|date',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
        ];

    }//end of onStore function

    public function onUpdate()
    {
        return [
            'name' => 'required|string',
            'details' => 'required|string',
            'price' => 'required|integer',
            'photo' => 'required|file',
            'start_at' => 'required|date',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
        ];

    }//end of onUpdate function
}
