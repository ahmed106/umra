<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $guarded = [];
    protected $appends = ['image'];


    public function logo()
    {

        return $this->morphOne(Photo::class, 'photoable');

    }//end of photo function

    public function getImageAttribute()
    {

        if ($this->logo == '') {
            return asset('default.svg');
        }

        return asset('images/settings/' . $this->logo->src);


    }//end of getImageAttribute function
}
