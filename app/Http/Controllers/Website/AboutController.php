<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Customer;
use App\Models\Page;

class AboutController extends Controller
{
    public function index()
    {
        $about = About::with('photo')->first();
        $page_name =Page::where('url','/about')->first()->name;
        return view('website.pages.about.index', compact('about','page_name'));
    }//end of index function
}
