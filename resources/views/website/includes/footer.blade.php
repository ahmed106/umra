<div class="wpo-ne-footer">
    <footer class="wpo-site-footer">
        <div class="wpo-upper-footer">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-3 col-md-3 col-sm-6">
                        <div class="widget about-widget">
                            <div class="logo widget-title">
                                <img src="{{$setting->image}}" alt="blog">
                            </div>
                            <p>
                                تسهيل حجز العمرة وتقديم كافة التسهيلات لإجراء العمرة .
                            </p>
                            <ul>
                                <li><a href="{{$setting?$setting->website_facebook:''}}"><i class="ti-facebook"></i></a></li>
                                <li><a href="{{$setting?$setting->website_twitter:''}}"><i class="ti-twitter-alt"></i></a></li>

                                <li><a href="{{$setting?$setting->website_linked_in:''}}"><i class="ti-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col col-lg-3 col-md-3 col-sm-6">
                        <div class="widget link-widget">
                            <div class="widget-title">
                                <h3>خدماتنا</h3>
                            </div>
                            <ul>
                                @foreach($services as $service)
                                    <li><a href="{{url('services/'.$service->id)}}">{{$service->title}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col col-lg-2 col-md-3 col-sm-6">
                        <div class="widget link-widget">
                            <div class="widget-title">
                                <h3>روابط سريعة</h3>
                            </div>
                            <ul>
                                @foreach($pages->take(4) as $page)
                                    <li><a href="{{$page->url}}">{{$page->name}}</a></li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <div class="col col-lg-3 col-lg-offset-1 col-md-3 col-sm-6">
                        <div class="widget market-widget wpo-service-link-widget">
                            <div class="widget-title">
                                <h3>تواصل معنا </h3>
                            </div>

                            <div class="contact-ft">
                                <ul>
                                    <li><i class="fi ti-location-pin"></i><span>{{$setting?$setting->website_address:''}}</span></li>
                                    <li><i class="fi flaticon-call"></i><span>{{$setting?$setting->website_phone:''}}</span></li>
                                    <li><i class="fi flaticon-envelope"></i><span>{{$setting?$setting->website_email:''}}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </div>
        <div class="wpo-lower-footer">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <p class="copyright">&copy; 2022 Umrah. جميع الحقوق محفوظة</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
