@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">
                <div class="card">
                    <div class="card-header">
                        <h3>تفاصيل الطلب</h3>
                    </div>
                    <div class="card-body">
                        <div class="umrah-details">
                            <div class="right-side">
                                <img src="https://www.makaremhotels.com/sites/default/files/styles/de2e_traditional/public/2021-12/Blog-Image-1.jpg?h=d1cb525d&itok=mGnF-kho" alt="">
                                <h3>{{$booking->umra->name}}</h3>
                                <div class="desc">
                                    {!! $booking->umra->details !!}
                                </div>
                                <div class="fund-detail">
                                   <div class="fund-item">
                                      <span class="fund-name">سعر العمرة</span>
                                      <span class="fund-content">{{$booking->umra->price}} ر.س</span>
                                   </div>
                                   <div class="fund-item">
                                      <span class="fund-name">تاريخ الانطلاق</span>
                                      <span class="fund-content">{{\Carbon\Carbon::parse($booking->umra->start_at)->format('M, Y d')}}</span>
                                   </div>
                                </div>
                            </div>
                            <div class="left-side">
                                <h3>بيانات الراسل</h3>
                                <ul>
                                    <li>
                                        <strong>الاسم</strong>
                                        <p>{{$booking->name}}</p>
                                    </li>
                                    <li>
                                        <strong>الهاتف</strong>
                                        <p>{{$booking->phone}}</p>
                                    </li>
                                    <li>
                                        <strong>البريد الالكتروني</strong>
                                        <p>{{$booking->email}}</p>
                                    </li>
                                    <li>
                                        <strong>الدولة</strong>
                                        <p>{{$booking->country}}</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->

@endsection
