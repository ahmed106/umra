<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Page;
use App\Models\Umra;
use Illuminate\Http\Request;

class UmraController extends Controller
{
    public function index()
    {
        $page_name = Page::where('url', '/umra')->first()->name;
        $umras = Umra::with('photo')->get();
        return view('website.pages.umra.index', compact('umras', 'page_name'));
    }//end of index function

    public function show($id)
    {
        $umra = Umra::findOrFail($id);
        $page_name = Page::where('url', '/umra')->first()->name;
        return view('website.pages.umra.show', compact('umra', 'page_name'));
    }//end of show function

    public function booking(Request $request)
    {

       $data =  $request->validate([
            'umra_id'=>'required|exists:umras,id',
            'name'=>'required|string',
            'email'=>'required|email',
            'phone'=>'required',
            'country'=>'required|string',
        ]);

       Booking::create($data);

       return redirect()->back()->with('success','تم حجز عمرتك بنجاح');
    }//end of booking function
}
