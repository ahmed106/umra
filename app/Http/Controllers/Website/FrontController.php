<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Blog;
use App\Models\Customer;

use App\Models\Page;
use App\Models\Service;
use App\Models\Slider;
use App\Models\Umra;

class FrontController extends Controller
{
    public function index()
    {


        $page_title =Page::whereUrl('/')->first()->name;
        $sliders = Slider::with('photo')->orderBy('id', 'desc')->get();
        $services = Service::with('photo')->get();
        $customers = Customer::with('photo')->get();
        $blogs = Blog::with('photo')->get();
        $about = About::with('photo')->first();
        $umras = Umra::with('photo')->get()->take(2);
        return view('home', compact('sliders', 'services', 'customers', 'blogs', 'about','umras','page_title'));

    }//end of index function
}
