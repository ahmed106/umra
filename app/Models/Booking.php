<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table  = 'bookings';
    protected $guarded = [];

    public function umra(){
            return $this->belongsTo(Umra::class,'umra_id');

    }//end of umra function
}
