<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::namespace('Website')->group(function () {

    // home
    Route::get('/', 'FrontController@index')->name('home');
    // end home

    // about
    Route::get('about', 'AboutController@index');
    // end about

    // services
    Route::get('services', 'ServiceController@index');
    Route::get('services/{id}', 'ServiceController@show');
    // end services

    // projects
    Route::get('projects', 'ProjectController@index');
    Route::get('projects/{id}', 'ProjectController@show');
    // end projects

    // blogs
    Route::get('blogs', 'BlogController@index');
    Route::get('blogs/{id}', 'BlogController@show');
    // end blogs

    //contact us
    Route::view('contact-us', 'website.pages.contact_us.index');
    Route::post('contact-us', 'ContactUsController@post')->name('website.contactUs');
    // end contact us

    // umra
    Route::get('umra', 'UmraController@index');
    Route::get('umra/{id}', 'UmraController@show');
    Route::post('umra/booking', 'UmraController@booking')->name('umra.booking');
    // end umra

    Route::get('contact-us','ContactUsController@index');
    Route::post('contact-us','ContactUsController@post')->name('contact_us.save');

});

