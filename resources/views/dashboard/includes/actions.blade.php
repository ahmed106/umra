<div class="table-btns">
        @if($model !='bookings')
        <a href="{{route($model.'.edit',$raw->id)}}" class="btn ripple btn-info btn-icon" data-bs-placement="top" data-bs-toggle="tooltip" data-bs-original-title="تعديل"><i class="fa {{$model == 'contact-us' ? 'fa-eye':'fa-edit' }} "></i></a>
    @else
        <a href="{{route($model.'.show',$raw->id)}}" class="btn ripple btn-info btn-icon" data-bs-placement="top" data-bs-toggle="tooltip" data-bs-original-title="تفاصيل"><i class="fa fa-eye"></i></a>

    @endif
    @if($model !='pages')

        <a id="delete_btn" href="javascript:void();" class="btn ripple btn-danger btn-icon" data-bs-placement="top" data-bs-toggle="tooltip" data-bs-original-title="حذف"><i class="fe fe-trash"></i></a>
    @endif
    <form id="delete_form" action="{{route($model.'.destroy',$raw->id)}}" method="post">
        @csrf
        @method('delete')

    </form>
</div>





