@extends('website.layouts.master')
@push('title')
    {{$page_name}}
@endpush
@section('content')
    <div class="wpo-breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-breadcumb-wrap">
                        <h2>{{$page_name}}</h2>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسيه</a></li>
                            <li><span>{{$page_name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="service-single-section section-padding">
        <div class="container">
            <div class="row flex-row">
                <div class="col col-md-8 col-xs-12">
                    <div class="service-single-content">
                        <div class="service-single-img">
                            <img src="{{$umra->image}}" alt>
                        </div>
                        <h2>{{$umra->name}}</h2>
                        <p>
                            {!! $umra->details !!}
                        </p>
                    </div>
                </div>
                <div class="col col-md-4 col-xs-12">
                    <div class="umrah-sidebar">
                        <ul class="price-date">
                            <li>
                                <p>سعر الرحلة</p>
                                <span>{{$umra->price}} ر.س</span>
                            </li>
                            <li>
                                <p>تاريخ الانطلاق</p>
                                <span>{{\Carbon\Carbon::parse($umra->start_at)->format('d/m/Y')}}</span>
                            </li>
                        </ul>
                        <form action="{{route('umra.booking')}}" class="booking" method="post">
                            @csrf
                            <input type="hidden" name="umra_id" value="{{$umra->id}}">
                            <h4>احجز الآن</h4>
                            <ul>
                                <li>
                                    <input type="text" name="country" required class="form-control" placeholder="الدولة">
                                </li>
                                <li>
                                    <input type="text" name="name" required class="form-control" placeholder="الاسم بالكامل">
                                </li>
                                <li>
                                    <input type="text" name="phone" required class="form-control" placeholder="رقم الهاتف">
                                </li>
                                <li>
                                    <input type="email" name="email" required class="form-control" placeholder="البريد الالكتروني">
                                </li>
                                <li>
                                    <button type="submit" class="theme-btn">ارسال</button>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div> <!-- end row -->
        </div> <!-- end container -->
    </section>
@endsection
