@extends('website.layouts.master')
@push('title')
    {{$page_name}} |  {{$blog->title}}
@endpush
@section('content')

    <div class="wpo-breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-breadcumb-wrap">
                        <h2>{{$page_name}}</h2>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسيه</a></li>
                            <li><span>{{$page_name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="wpo-blog-single-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col col-md-8 col-xs-12">
                    <div class="wpo-wpo-blog-content clearfix">
                        <div class="post">
                            <div class="post format-standard-image">
                                <div class="entry-media">
                                    <img src="{{$blog->image}}" alt>
                                </div>
                                <ul class="entry-meta">
                                    <li>كاتب الموضوع :  {{$blog->creator->name}}</li>
                                    <li>{{\Carbon\Carbon::parse($blog->created_at)->format('M Y d')}}</li>
                                </ul>
                            </div>
                            <h2>{{$blog->title}}</h2>
                            <p>{!! $blog->content !!}</p>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 col-xs-12">
                    <div class="wpo-blog-sidebar">
                        <div class="widget recent-post-widget">
                            <h3>مقالات اخرى</h3>
                            <div class="posts">
                                @foreach($blogs_ as $blog)
                                    <a href="{{url('blogs/'.$blog->id)}}" class="post">
                                        <div class="img-holder">
                                            <img src="{{$blog->image}}" alt>
                                        </div>
                                        <div class="details">
                                            <h4>{{$blog->title}}</h4>
                                            <span class="date">{{\Carbon\Carbon::parse($blog->created_at)->format('M Y d')}}</span>
                                        </div>
                                    </a>
                                @endforeach


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end container -->
    </section>
@endsection
