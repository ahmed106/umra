@extends('website.layouts.master')
@push('title')
    {{$page_name}}
@endpush
@section('content')
    <div class="wpo-breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-breadcumb-wrap">
                        <h2>{{$page_name}}</h2>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسيه</a></li>
                            <li><span>{{$page_name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wpo-event-area-2 section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-section-title">
                        <span>{{$page_name}}</span>
                        <h2>رحلات العمرة المتاحة</h2>
                    </div>
                </div>
            </div>
            <div class="charity-inner">
                <div class="row">
                    @foreach($umras as $umra)

                        <div class="col-md-6">
                            <a href="{{url('umra/'.$umra->id)}}" class="charity-item">
                                <figure class="charity-image">
                                    <img src="{{$umra->image}}" alt="">
                                </figure>
                                <div class="charity-content">
                                    <h3>{{$umra->name}}</h3>
                                    <div class="desc">{!! $umra->details !!}</div>
                                    <div class="fund-detail bg-light-grey">
                                        <div class="fund-item">
                                            <span class="fund-name">سعر الرحلة</span>
                                            <span class="fund-content">{{$umra->price}} ر.س</span>
                                        </div>
                                        <div class="fund-item">
                                            <span class="fund-name">تاريخ الانطلاق</span>
                                            <span class="fund-content">{{\Carbon\Carbon::parse($umra->start_at)->format('M, Y d')}}</span>
                                        </div>
                                    </div>
                                    <div class="btn-wrap">
                                        <span>مزيد من التفاصيل <i class="fa fa-angle-double-right"></i></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach


                </div>
            </div>
        </div>
    </div>
@endsection
