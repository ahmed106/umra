<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Http\Requests\UmraRequest;
use App\Models\Umra;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UmraController extends Controller
{
    use UploadTrait;

    public function index()
    {

        return view('dashboard.umras.index');
    }


    public function create()
    {
        return view('dashboard.umras.create');
    }

    public function data()
    {
        $model = 'umras';
        $Umras = Umra::with('photo')->get();
        return DataTables::of($Umras)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" class="check_item" value="' . $raw->id . '">';
            })
            ->addcolumn('actions', function ($raw) use ($model) {

                return view('dashboard.includes.actions', compact('raw', 'model'));
            })
            ->rawColumns(['check_item' => 'check_item'])
            ->make(true);

    }//end of data function


    public function store(UmraRequest $request)
    {

        $data = $request->validated();


        unset($data['photo']);


        $Umra = Umra::create($data);

        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'umras');
            $Umra->photo()->create([
                'src' => $photo,
                'type' => 'Umra',
            ]);

        };

        return redirect()->route('umras.index')->with('success', 'تم إضافه البيانات بنجاح');

    }


    public function edit(Umra $umra)
    {
        return view('dashboard.umras.edit', compact('umra'));
    }

    public function update(UmraRequest $request, Umra $umra)
    {

        $data = $request->validated();
        unset($data['photo']);
        $umra->update($data);

        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'umras');
            $umra->photo ? $this->deleteOldPhoto('images/umras/', $umra->photo->src)
                & $umra->photo()->update(['src' => $photo]) : $umra->photo()->create(['src' => $photo, 'type' => 'Umra']);
        }

        return redirect()->route('umras.index')->with('success', 'تم تعديل البيانات بنجاح');
    }


    public function destroy(Umra $Umra)
    {
        $Umra->photo ? $this->deleteOldPhoto('images/umras/', $Umra->photo->src) & $Umra->photo()->delete() : '';
        Umra::destroy($Umra->id);
        return redirect()->route('umras.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request->items as $item) {
            $Umra = Umra::findOrFail($item);
            $this->destroy($Umra);
        }
        Umra::destroy($request->items);
        return redirect()->route('umras.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function


}
