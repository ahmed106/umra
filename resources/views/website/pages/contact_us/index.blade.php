@extends('website.layouts.master')
@push('title')
    {{$page_name}}
@endpush
@section('content')
    <div class="wpo-breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-breadcumb-wrap">
                        <h2>{{$page_name}}</h2>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسيه</a></li>
                            <li><span>{{$page_name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="wpo-contact-form-map section-padding">
        <div class="container">
            <div class="row">
                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="contact-form">
                        <h2>تواصل معنا</h2>
                        <form method="post" action="{{route('contact_us.save')}}" >
                            @csrf
                            <div>
                                <input type="text" class="form-control" name="name"  placeholder="الاسم">
                            </div>
                            <div>
                                <input type="text" class="form-control" name="phone"  placeholder="رقم الهاتف">
                            </div>
                            <div>
                                <input type="email" class="form-control" name="email" placeholder="البريد الالكتروني">
                            </div>
                            <div>
                                <textarea class="form-control" name="body"   placeholder="رسالتك"></textarea>
                            </div>
                            <div class="submit-area">
                                <button type="submit" class="theme-btn submit-btn">ارسال</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="contact-map">
                        {!! $setting?$setting->map:'' !!}
                    </div>
                </div>
            </div>

            <div class="wpo-contact-info">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="info-item">
                            <h2>{{$setting?$setting->website_address:''}}</h2>
                            <div class="info-wrap">
                                <div class="info-icon">
                                    <i class="ti-location-pin"></i>
                                </div>
                                <div class="info-text">
                                    <span>العنوان</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="info-item">
                            <h2>{{$setting?$setting->website_email:''}}</h2>
                            <div class="info-wrap">
                                <div class="info-icon-2">
                                    <i class="fi flaticon-envelope"></i>
                                </div>
                                <div class="info-text">
                                    <span>البريد الالكتروني</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="info-item">
                            <h2>{{$setting?$setting->website_phone:''}}</h2>
                            <div class="info-wrap">
                                <div class="info-icon-3">
                                    <i class="ti-headphone-alt"></i>
                                </div>
                                <div class="info-text">
                                    <span>الهاتف</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end container -->
    </section>
@endsection


