<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Customer;
use App\Models\Page;

class BlogController extends Controller
{
    public function index()
    {
        $page_name = Page::where('url', '/blogs')->first()->name;
        $blogs = Blog::with('photo')->get();


        return view('website.pages.blogs.index', compact('blogs', 'page_name'));

    }//end of index function

    public function show($id)
    {
        $blog = Blog::with('photo')->findOrFail($id);
        $blogs_ = Blog::with('photo')->where('id', '!=', $id)->get();
        $page_name = Page::where('url','/blogs')->first()->name;
        return view('website.pages.blogs.show', compact('blog',  'blogs_','page_name'));

    }//end of show function
}
