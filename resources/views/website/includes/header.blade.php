<!-- start preloader -->
<div class="preloader">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>
<!-- end preloader -->
<header id="header" class="wpo-site-header wpo-header-style-2">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col col-md-6 col-sm-7 col-12">
                    <div class="contact-intro">
                        <ul>
                            <li><i class="fi ti-location-pin"></i>{{$setting?$setting->website_address:''}}</li>
                            <a href="mailTo:{{$setting?$setting->website_email:''}}">
                                <li><i class="fi flaticon-envelope"></i> {{$setting?$setting->website_email:''}}</li>
                            </a>
                        </ul>
                    </div>
                </div>
                <div class="col col-md-6 col-sm-5 col-12">
                    <div class="contact-info">
                        <ul>
                            <li><a href="{{$setting?$setting->website_facebook:''}}"><i class="ti-facebook"></i></a></li>
                            <li><a href="{{$setting?$setting->website_twitter:''}}"><i class="ti-twitter-alt"></i></a></li>
                            <li><a href="{{$setting?$setting->website_linked_in:''}}"><i class="ti-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end topbar -->
    <div class="site-header header-style-1">
        <nav class="navigation navbar navbar-default original">
            <div class="container">
                <div class="flex-header">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>



                            <a class="navbar-brand" href="{{url('/')}}"><img src="{{$setting->image}}" alt=""></a>

                    </div>
                    <div id="navbar" class="navbar-collapse collapse navigation-holder">
                        <button class="close-navbar"><i class="ti-close"></i></button>
                        <ul class="nav navbar-nav">
                            @foreach($pages as $page)
                                <li><a href="{{$page->url}}">{{$page->name}}</a></li>
                            @endforeach


                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
