@extends('website.layouts.master')

@push('title')
    {{$page_name}}
@endpush
@section('content')
    <div class="wpo-breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-breadcumb-wrap">
                        <h2>{{$page_name}}</h2>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسيه</a></li>
                            <li><span>{{$page_name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blog-area section-padding">
        <div class="container">
            <div class="col-l2">
                <div class="wpo-section-title">
                <span>المقالات</span>
                    <h2>آخر الأخبار والمقالات</h2>
                </div>
            </div>
            <div class="row">
                @foreach($blogs as $blog)
                    <div class="col-lg-4 col-sm-6 col-12 custom-grid">
                        <a href="{{url('blogs/'.$blog->id)}}" class="blog-item">
                            <div class="blog-img">
                                <img src="{{$blog->image}}" alt="">
                            </div>
                            <div class="blog-content">
                                <h3>{{$blog->title}}</h3>
                                <ul class="post-meta">
                                    <li><img src="{{asset('images/profile/'.$blog->creator->photo)}}" alt="">{{$blog->creator->name}}</li>
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> {{\Carbon\Carbon::parse($blog->created_at)->format('M Y d')}}</li>
                                </ul>
                            </div>
                        </a>
                    </div>
                @endforeach


            </div>
        </div>
    </div>
@endsection
