
<script src="{{asset('assets/website/')}}/js/jquery.min.js"></script>
<script src="{{asset('assets/website/')}}/js/bootstrap.min.js"></script>
<script src="{{asset('assets/website/')}}/js/circle-progress.min.js"></script>
<script src="{{asset('assets/website/')}}/js/jquery-plugin-collection.js"></script>
<script src="{{asset('assets/website/')}}/js/script.js"></script>


<script src="{{asset('assets/dashboard/plugins/toastr/toastr.js')}}"></script>
<script src="{{asset('assets/dashboard/plugins/sweet_alert/swal.js')}}"></script>
@include('dashboard.includes.swal')
@toastr_render
