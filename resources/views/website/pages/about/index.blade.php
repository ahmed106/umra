@extends('website.layouts.master')
@push('title')
    {{$page_name}}
@endpush
@section('content')
    <div class="wpo-breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-breadcumb-wrap">
                        <h2>{{$page_name}}</h2>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسيه</a></li>
                            <li><span>{{$page_name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wpo-about-area-2 section-padding">
        <div class="container">
            <div class="wpo-about-wrap">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="wpo-about-img-3">
                            <img src="{{$about?$about->image:''}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="wpo-about-text">
                            <div class="wpo-section-title">
                                <span>{{$page_name}}</span>
                                <h2>{{$about?$about->title:''}}</h2>
                            </div>
                            <p>
                                {!! $about?$about->content:'' !!}
                            </p>
                            <p>
                                {!! $about?$about->vision:'' !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
