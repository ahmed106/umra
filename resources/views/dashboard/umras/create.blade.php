@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('umras.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h3>إضافه عمره</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{old('name')}}" type="text" name="name" class="form-control">
                                        <span class="input-span">اسم العمره</span>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <textarea name="details" rows="3" class="form-control editor">{{old('details')}}</textarea>
                                        <span class="input-span">تفاصيل العمره</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input type="date" value="{{old('start_at')}}" name="start_at" class="form-control">
                                        <span class="input-span">تاريخ الانطلاق</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input type="number" min="0" value="{{old('price')}}" name="price" class="form-control">
                                        <span class="input-span">سعر العمره</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <label class="">الصورة ( مقاس الصورة -- 750 طول * 1660 عرض )</label>
                                        <input id="photo" type="file" name="photo">
                                        <img width="100" id="preview" height="100" src="{{asset('default.svg')}}" alt="">
                                    </div>

                                </div>


                            </div>

                            <div class="accordion-container">
                                <div class="set">
                                    <a href="#">
                                        بيانات السيو SEO
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="box-custom">
                                        <div class="flex-divs">
                                            <div class="input-custom">
                                                <input value="" type="text" name="meta_title" class="form-control">
                                                <span class="input-span">عنوان الميتا</span>
                                            </div>
                                            <div class="input-custom">
                                                <input value="" type="text" name="meta_keywords" class="form-control">
                                                <span class="input-span">الكلمات الدلالية</span>
                                            </div>
                                            <div class="input-custom">
                                                <textarea name="meta_description" class="form-control editor"></textarea>
                                                <span class="input-span">وصف الميتا</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
@push('js')

@endpush
