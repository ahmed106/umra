<section class="hero hero-style-1">
    <div class="hero-slider" dir="rtl">
        @foreach($sliders as $slider)
            <div class="slide">
                <div class="container">
                    <img src="{{$slider->image}}" alt class="slider-bg">
                    <div class="row">
                        <div class="col col-md-8 slide-caption">
                            <div class="slide-top">
                                <span>{{$slider->title}}</span>
                            </div>
                            <div class="slide-title">
                                <h2>{{$slider->content}}</h2>
                            </div>
                            <div class="btns">
                                <a href="{{url('/about')}}" class="theme-btn">من نحن</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach


    </div>
</section>
