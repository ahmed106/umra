@extends('dashboard.layouts.master')
@section('content')

    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('user.editProfile')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h3>تعديل الصفحه الشخصيه</h3>
                        </div>


                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{auth()->user()->name}}" type="text" name="name" class="form-control">
                                        <span class="input-span">الاسم</span>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{auth()->user()->email}}" type="email" name="email" class="form-control">
                                        <span class="input-span">البريد الالكتروني</span>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input type="password" name="password" class="form-control">
                                        <span class="input-span">كلمه المرور</span>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="input-custom">
                                        <label class="">الصورة ( مقاس الصورة -- 100 طول * 100 عرض )</label>
                                        <input id="photo" type="file" name="photo">
                                        <img width="100" id="preview" height="100" src="{{asset('images/profile/'.auth()->user()->photo)}}" alt="">
                                        {{--                                        <img width="100" id="preview" height="100" src="{{asset('default.svg')}}" alt="">--}}
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
