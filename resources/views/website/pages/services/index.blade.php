@extends('website.layouts.master')
@push('title')
    {{$page_name}}
@endpush
@section('content')
    <div class="wpo-breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-breadcumb-wrap">
                        <h2>{{$page_name}}</h2>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسيه</a></li>
                            <li><span>{{$page_name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="service-area-2 section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-section-title">
                        <span>خدمات نقدمها لك</span>
                        <h2>{{$page_name}}</h2>
                    </div>
                </div>
            </div>
            <div class="service-wrap">
                <div class="row">
                    @foreach($services as $service)
                        <div class="col-lg-4 col-md-4 col-sm-6 custom-grid col-12">
                            <a href="{{url('services/'.$service->id)}}" class="service-single-item">
                                <div class="service-single-img">
                                    <img src="{{$service->image}}" alt="">
                                </div>
                                <div class="service-text">
                                    <h2>{{$service->title}}</h2>
                                </div>
                            </a>
                        </div>
                    @endforeach


                </div>
            </div>
        </div>
    </div>
@endsection
