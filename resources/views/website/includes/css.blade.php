<link rel="icon" href="{{$setting?$setting->image:''}}" type="image/x-icon">
<link href="{{asset('assets/website/')}}//css/themify-icons.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/font-awesome.min.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/flaticon.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/bootstrap.min.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/animate.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/owl.carousel.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/owl.theme.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/slick.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/slick-theme.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/swiper.min.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/owl.transitions.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/jquery.fancybox.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/odometer-theme-default.css" rel="stylesheet">
<link href="{{asset('assets/website/')}}//css/nice-select.css" rel="stylesheet">
<!-- <link href="{{asset('assets/website/')}}//css/style.css" rel="stylesheet"> -->
<link href="{{asset('assets/website/')}}//css/style-rtl.css?v=1" rel="stylesheet">

<link rel="stylesheet" href="{{asset('assets/dashboard/plugins/toastr/toastr.css')}}">
@toastr_css
