@extends('website.layouts.master')
@push('title')
    {{$page_title}}
@endpush

@section('content')
    @include('website.includes.sliders')
    <!-- end of hero slider -->
    <!-- wpo-about-area start -->
    <div class="wpo-about-area-2 section-padding">
        <div class="container">
            <div class="wpo-about-wrap">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="wpo-about-img-3">
                            <img src="{{$about?$about->image:''}}" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="wpo-about-text">
                            <div class="wpo-section-title">
                                <span>من نحن</span>
                                <h2>{{$about?$about->title:''}}</h2>
                            </div>
                            <p>{!! $about?$about->content:'' !!}</p>
                            <div class="btns">
                                <a href="{{url('/about')}}" class="theme-btn" tabindex="0">اقرأ المزيد</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- wpo-about-area end -->
    <!-- service-area-start -->
    <div class="service-area-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-section-title">
                        <span>خدماتنا</span>
                        <h2>خدمات نقدمها لك</h2>
                    </div>
                </div>
            </div>
            <div class="service-wrap">
                <div class="row">
                    @foreach($services as $service)
                        <div class="col-lg-4 col-md-4 col-sm-6 custom-grid col-12">
                            <a href="{{url('services/'.$service->id)}}" class="service-single-item">
                                <div class="service-single-img">
                                    <img src="{{$service->image}}" alt="">
                                </div>
                                <div class="service-text">
                                    <h2>{{$service->title}}</h2>
                                </div>
                            </a>
                        </div>
                    @endforeach


                </div>
            </div>
        </div>
    </div>
    <!-- service-area-end -->
    <!-- wpo-event-area start -->
    <div class="wpo-event-area-2 section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-section-title">
                        <span>العمرة</span>
                        <h2>اقرب رحلات العمرة</h2>
                    </div>
                </div>
            </div>
            <div class="charity-inner">
                <div class="row">
                    @foreach($umras as $umra)
                        <div class="col-md-6">
                            <a href="{{url('umra/'.$umra->id)}}" class="charity-item">
                                <figure class="charity-image">
                                    <img src="{{$umra->image}}" alt="">
                                </figure>
                                <div class="charity-content">
                                    <h3>{{$umra->name}}</h3>
                                    <div class="desc">{!! $umra->details !!}</div>
                                    <div class="fund-detail bg-light-grey">
                                        <div class="fund-item">
                                            <span class="fund-name">سعر الرحلة</span>
                                            <span class="fund-content">{{$umra->price}} ر.س</span>
                                        </div>
                                        <div class="fund-item">
                                            <span class="fund-name">تاريخ الانطلاق</span>
                                            <span class="fund-content">{{\Carbon\Carbon::create($umra->start_at)->format('M, Y d')}}</span>
                                        </div>
                                    </div>
                                    <div class="btn-wrap">
                                        <span>مزيد من التفاصيل <i class="fa fa-angle-double-right"></i></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach


                </div>
                <div class="list-more-btn text-center">
                    <a href="{{url('/umra')}}" class="theme-btn">عرض الكل</a>
                </div>
            </div>
        </div>
    </div>
    <!-- wpo-event-area end -->

    <!-- blog-area start -->
    <div class="blog-area section-padding">
        <div class="container">
            <div class="col-l2">
                <div class="wpo-section-title">
                    <span>المقالات</span>
                    <h2>آخر الأخبار والمقالات</h2>
                </div>
            </div>
            <div class="row">
                @foreach($blogs->take(3) as $blog)
                    <div class="col-lg-4 col-sm-6 col-12 custom-grid">
                        <a href="{{url('blogs/'.$blog->id)}}" class="blog-item">
                            <div class="blog-img">
                                <img src="{{$blog->image}}" alt="">
                            </div>
                            <div class="blog-content">
                                <h3>{{$blog->title}}</h3>
                                <ul class="post-meta">

                                    <li><img src="{{asset('images/profile/'.$blog->creator->photo)}}" alt="">{{ucfirst($blog->creator->name)}}</li>
                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> {{\Carbon\Carbon::parse($blog->created_at)->format('M Y d')}}</li>
                                </ul>
                            </div>
                        </a>
                    </div>
                @endforeach


            </div>
        </div>
    </div>
    <!-- blog-area start -->
    <!-- footer-area start -->

@endsection
