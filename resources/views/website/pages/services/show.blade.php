@extends('website.layouts.master')
@push('title')
    خدماتنا |     {{$service->title}}
@endpush
@section('content')
    <div class="wpo-breadcumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="wpo-breadcumb-wrap">
                        <h2>{{$page_name}}</h2>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسيه</a></li>
                            <li><span>{{$page_name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="service-single-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col col-md-8 col-xs-12">
                    <div class="service-single-content">
                        <div class="service-single-img">
                            <img src="{{$service->image}}" alt>
                        </div>
                        <h2>{{$service->title}}</h2>
                        <p>
                            {!! $service->content !!}
                        </p>
                    </div>
                </div>
                <div class="col col-md-4 col-xs-12">
                    <div class="service-sidebar">
                        <div class="widget service-list-widget">
                            <h3>{{$page_name}}</h3>
                            <ul>
                                @foreach($services as $ser)

                                    <li class="{{$ser->id == $service->id ? 'current':''}}"><a href="{{url('/services/'.$ser->id)}}">{{$ser->title}}</a></li>
                                @endforeach

                            </ul>
                        </div>
                        <div class="widget contact-widget">
                            <div>
                                <h4>تواصل معنا</h4>
                                <h2>{{$setting?$setting->website_phone:""}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end row -->
        </div> <!-- end container -->
    </section>
@endsection
